#ifndef LIGHTS_H
#define LIGHTS_H
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>
#include "Shader.h"

class DirLight
{
public:
	glm::vec3 direction;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

public:
	DirLight();
	~DirLight();
	void apply(Shader* sp);
};

class SpotLight
{
public:
	glm::vec3 position;
	glm::vec3 direction;

	float outerCutOff;
	float cutOff;
	

	float constant;
	float linear;
	float quadratic;

	glm::vec3 diffuse;
	glm::vec3 specular;
	glm::vec3 ambient;
	

public:
	SpotLight();
	~SpotLight();
	void apply(Shader* sp);
};


#endif
