#define STB_IMAGE_IMPLEMENTATION
#define STBI_HEADER_FILE_ONLY
#define _CRT_SECURE_NO_WARNINGS

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <vector>
#include <cstdlib>

#include "Shader.h"
#include "Camera.h"

#include "stb_image.h"
#include "Lights.h"
#include "Renderable.h"


const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
const int FRAMES_PER_SECOND = 120;
const int SKIP_TICKS = 1000 / FRAMES_PER_SECOND;
glm::vec3 CLEAR_COLOR = glm::vec3(0.02f, 0.03f, 0.03f);
glm::vec3 Test = glm::vec3(0.00f, 0.00f, 0.00f);


void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
void processInput(GLFWwindow* window);

Camera* pCamera = nullptr;

Shader *m_lightPosShader, *m_shader;

DirLight* m_lightDir;
SpotLight* m_lightSpot;
Renderable* m_object;
std::vector<Renderable*> m_aquarium;
std::vector<Renderable*> bubbles;

int m_rockNumber = 45;
int m_plantNumber = 40;
int m_fishNumber = 30;

int objNum = m_rockNumber  + m_fishNumber + m_plantNumber;

bool enableF = 0, enableD = 1, enableS = 1;
float KEY_PRESS = 0.0;

float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

float deltaTime = 0.0f;
float lastFrame = 0.0f;



template<class T>
T random(T min, T max) {

	using dist = std::conditional_t<
		std::is_integral<T>::value,
		std::uniform_int_distribution<T>,
		std::uniform_real_distribution<T>
	>;
	return dist{ min, max }(gen);
}


void initOpenGLProgram(GLFWwindow* window)
{
	glClearColor(CLEAR_COLOR.x, CLEAR_COLOR.y, CLEAR_COLOR.z, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetMouseButtonCallback(window, mouseButtonCallback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL); // tell GLFW to capture our mouse
}

void freeOpenGLProgram(GLFWwindow* window)
{
	glfwDestroyWindow(window);
	for (int i = 0; i < objNum; i++)
	{
		Renderable* r = m_aquarium.back();
		m_aquarium.pop_back();
		delete r;
	}

}

void drawScene(GLFWwindow* window)
{
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_shader->use();
	m_shader->setVec3("viewPos", pCamera->GetPosition());
	m_shader->setFloat("material.shininess", 32.0f);

	m_lightDir->apply(m_shader);
	m_lightSpot->apply(m_shader);

	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 view = glm::mat4(1.0f);
	glm::mat4 projection = glm::mat4(1.0f);
	view = pCamera->GetViewMatrix();
	projection = glm::perspective(glm::radians(pCamera->GetZoom()), (float)SCR_WIDTH / (float)SCR_HEIGHT, 1.1f, 100.0f);

	m_shader->setMat4("view", view);
	m_shader->setMat4("projection", projection);
	m_shader->setMat4("model", model);
	m_shader->setVec3("FogColor", CLEAR_COLOR);

	m_shader->setBool("enableFog", enableF);
	m_shader->setBool("enableDirLight", enableD);
	m_shader->setBool("enableSpotLight", enableS);

	

	

	for (int i = 0; i < bubbles.size(); i++)
	{
		m_object = bubbles[i];
		m_object->behave();
		m_object->draw(m_shader);
	}


	for (int i = 0; i < objNum; i++)
	{
		m_object = m_aquarium[i];
		m_object->behave();
		m_object->draw(m_shader);
	}

	// creating aquarium 
	m_object = m_aquarium[objNum];
	m_object->draw(m_shader);

	//creating water
	m_object = m_aquarium[objNum + 1];
	m_object->draw(m_shader);

	//creating walls and floor

	m_object = m_aquarium[objNum + 2];
	m_object->draw(m_shader);

	m_object = m_aquarium[objNum + 3];
	m_object->draw(m_shader);

	m_object = m_aquarium[objNum + 4];
	m_object->draw(m_shader);

	m_object = m_aquarium[objNum + 5];
	m_object->draw(m_shader);

	m_object = m_aquarium[objNum + 6];
	m_object->draw(m_shader);

	m_object = m_aquarium[objNum + 7];
	m_object->draw(m_shader);

	glfwSwapBuffers(window);
}


int main()
{

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Aquarium", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}
	initOpenGLProgram(window);


	glEnable(GL_DEPTH_TEST);

	pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(0.0, 2.0, 7.0));


	Shader lightShader("normal.vs", "normal.fs");
	Shader defaultShader("lightEffects.vs", "lightEffects.fs");
	m_lightPosShader = &lightShader;
	m_shader = &defaultShader;


	DirLight light1;
	m_lightDir = &light1;

	SpotLight light2;
	m_lightSpot = &light2;

	for (int i = 0; i < m_rockNumber; i++)
	{
		Rock* rock = new Rock();
		m_aquarium.push_back(rock);
	}

	for (int i = 0; i < m_plantNumber; i++)
	{
		Plant* plant = new Plant();
		m_aquarium.push_back(plant);
	}

	for (int i = 0; i < m_fishNumber; i++)
	{
		Fish* fish = new Fish();
		m_aquarium.push_back(fish);
	}

	DWORD next_game_tick = GetTickCount64();
	int sleep_time = 0;

	Other* treasure = new Other( glm::vec3(1.0f, 0.0f, 1.0f), false);
	Other* water = new Other( glm::vec3(1.0f, 0.0f, 1.0f), true);

	Wall* wall1 = new Wall(glm::vec3(1.0f, 0.0f, 1.0f), 1);
	Wall* wall2 = new Wall(glm::vec3(1.0f, 0.0f, 1.0f), 2);
	Wall* wall3 = new Wall(glm::vec3(1.0f, 0.0f, 1.0f), 3);
	Wall* wall4 = new Wall(glm::vec3(1.0f, 0.0f, 1.0f), 4);
	Wall* floor = new Wall(glm::vec3(1.0f, 0.0f, 1.0f), 5);
	Wall* n203 = new Wall(glm::vec3(1.0f, 0.0f, 1.0f), 6);

	m_aquarium.push_back(treasure);
	m_aquarium.push_back(water);
	m_aquarium.push_back(wall1);
	m_aquarium.push_back(wall2);
	m_aquarium.push_back(wall3);
	m_aquarium.push_back(wall4);
	m_aquarium.push_back(floor);
	m_aquarium.push_back(n203);

	while (!glfwWindowShouldClose(window))
	{
		int randomBubblesChance = random(0, 10000);
		if (randomBubblesChance >= 9950)
		{
			Renderable* object;
			Bubble* buble = new Bubble(random(-3.f, 3.f), random(0.0f, 0.6f), random(-3.f, 3.f));
			bubbles.push_back(buble);
		}
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		processInput(window);
		drawScene(window);
		glfwPollEvents();

		next_game_tick += SKIP_TICKS;
		sleep_time = next_game_tick - GetTickCount64();
		if (sleep_time >= 0)
		{
			Sleep(sleep_time);
		}
	}
	freeOpenGLProgram(window);
	glfwTerminate();
	return 0;
}

void processInput(GLFWwindow* window)
{
	if (glfwGetTime() - KEY_PRESS > 0.35)
	{
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			pCamera->ProcessKeyboard(FORWARD, (float)deltaTime);
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			pCamera->ProcessKeyboard(BACKWARD, (float)deltaTime);
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			pCamera->ProcessKeyboard(LEFT, (float)deltaTime);
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			pCamera->ProcessKeyboard(RIGHT, (float)deltaTime);
		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
			pCamera->ProcessKeyboard(UP, (float)deltaTime);
		if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
			pCamera->ProcessKeyboard(DOWN, (float)deltaTime);
	
		

		if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS)
		{
			enableF = !enableF; KEY_PRESS = glfwGetTime();
		}
		if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS)
		{
			enableS = !enableS; KEY_PRESS = glfwGetTime();
		}
		if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_PRESS)
		{
			enableD = !enableD; KEY_PRESS = glfwGetTime();
		}
	}
	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		pCamera->Reset(width, height);

	}
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	pCamera->Reshape(width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	pCamera->MouseControl((float)xpos, (float)ypos);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yOffset)
{
	pCamera->ProcessMouseScroll((float)yOffset);
}

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		pCamera->EnableCamera = true;
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}
	else
	{
		pCamera->EnableCamera = false;

		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}