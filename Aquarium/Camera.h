#ifndef CAMERA_H
#define CAMERA_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <cmath>
#include <vector>

const float YAW = -90.0f;
const float PITCH = 0.0f;
const float SPEED = 2.5f;
const float SENSITIVITY = 0.1f;
const float ZOOM = 45.0f;
const float RADIUS = 8.0f;

enum ECameraMovementType
{
	UNKNOWN,
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

class Camera
{
private:
	glm::vec3 position;
	glm::vec3 forward;
	glm::vec3 right;
	glm::vec3 up;
	glm::vec3 worldUp;
	
	float yaw;
	float pitch;
	
	float movementSpeed;
	float Zoom;
	float Radius = 6.0f;

	const float zNEAR = 0.1f;
	const float zFAR = 500.f;
	const float YAW = -90.0f;
	const float PITCH = 0.0f;
	const float FOV = 45.0f;
	glm::vec3 startPosition;
    bool isPerspective;
    float zNear;
    float zFar;
    float FoVy;
    int width;
    int height;
	bool bFirstMouseMove = true;
	float lastX = 0.f, lastY = 0.f;
    const float cameraSpeedFactor = 2.5f;
    const float mouseSensitivity = 0.1f;
	
	void updateCameraVectors();

public:
    bool EnableCamera; 

    Camera(const int width, const int height, const glm::vec3& position);
    void Set(const int width, const int height, const glm::vec3& position);
    void Reset(const int width, const int height);
    void Reshape(int windowWidth, int windowHeight);
    const glm::vec3 GetPosition() const;
    const glm::mat4 GetViewMatrix() const;
    const float GetZoom() const;
    const glm::mat4 GetProjectionMatrix() const; 
    void ProcessKeyboard(ECameraMovementType direction, float deltaTime); 
    void MouseControl(float xPos, float yPos);
    void ProcessMouseScroll(float yOffset);

private:
    void ProcessMouseMovement(float xOffset, float yOffset, bool constrainPitch = true);  
    void UpdateCameraVectors();
        
};
#endif