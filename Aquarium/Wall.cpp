#include "Renderable.h"

#include "stb_image.h"

Wall::Wall(glm::vec3 initialPosition,int wallNo)
{
	Model = glm::mat4(1.0f);

	std::vector< glm::vec3 > vertices;
	std::vector< glm::vec2 > uvs;
	std::vector< glm::vec3 > normals;

	std::string path_obj;

	if(wallNo<=2)
		path_obj = "Models/Wall.obj";
	else if(wallNo!=5) path_obj = "Models/Wall2.obj";
	else path_obj = "Models/Floor.obj";

	if(wallNo==6)path_obj = "Models/203.obj";


	bool res = loadOBJ(path_obj.c_str(), vertices, uvs, normals);

	sx = sz = 30;
	sy = 15;

	switch (wallNo)
	{
	case 1:

		x = -20;
		y = 0;
		z = 0;

		break;
	case 2:
		x = 20;
		y = 0;
		z = 0;
		break;
	case 3:
		x = 0;
		y = 0;
		z = 20;
		break;
	case 4:
		x = 0;
		y = 0;
		z = -20;
		break;
	case 5:
		
		x = 0;
		y = 0;
		z = 0;
		break;

	case 6:
		sz = 6;
		sx = 6;
		sy = 6;
		x = 0;
		y = 6.5;
		z = -19;
		break;

	default:
		break;
	}



	VerticesNumber = vertices.size() * 3;

	

	glGenVertexArrays(1, &VAO);
	glGenBuffers(3, VBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, VBO[2]);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(2);

	glGenTextures(1, &Texture);
	glBindTexture(GL_TEXTURE_2D, Texture);
	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	std::string path_tex;
	
	if(wallNo==5)
		path_tex = "Models/parchet.jpg";
	else
		path_tex = "Models/wall3.jpg";
	TextureData = stbi_load(path_tex.c_str(), &TextureWidth, &TextureHeight, &nrChannels, 0);
	if (TextureData)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, TextureWidth, TextureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, TextureData);
		glGenerateMipmap(GL_TEXTURE_2D);

	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(TextureData);
}

void Wall::draw(Shader* sp)
{
	sp->use();
	this->Model = glm::mat4(1.0f);
	this->Model = glm::translate(this->Model, glm::vec3(x, y, z));
	this->Model = glm::scale(this->Model, glm::vec3(sx, sy , sz));
	sp->setMat4("model", this->Model);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, Texture);
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, VerticesNumber);
}

void Wall::behave()
{
}