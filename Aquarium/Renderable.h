#ifndef RENDERABLE_H
#define RENDERABLE_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>
#include <random>

#include "Shader.h"

extern std::mt19937 gen;

const float MAX_X = 9.0f;
const float MAX_Y = 5.0f;
const float MAX_Z = 5.5f;
const float MIN_Y = 1.3f;


class Renderable
{
protected:
	glm::mat4 Model;
		//pozitia obiectului
	float rx, ry, rz;	//rotatia obiectului
	float sx, sy, sz;	//marimea obiectului
	
	unsigned char* TextureData;
	int TextureWidth, TextureHeight, nrChannels;
	unsigned int Texture;

	int VerticesNumber;

	template<class T>
	T random(T min, T max);

public:
	Renderable();
	virtual ~Renderable() = 0;
	virtual void draw(Shader* sp) = 0;
	virtual void behave() = 0;
	virtual bool loadOBJ(const char* path,
	std::vector < glm::vec3 >& out_vertices,
	std::vector < glm::vec2 >& out_uvs,
	std::vector < glm::vec3 >& out_normals);

	float x, y, z;
};

template<class T>
T Renderable::random(T min, T max)
{
	using dist = std::conditional_t<
	std::is_integral<T>::value,
	std::uniform_int_distribution<T>,
	std::uniform_real_distribution<T>>;
	return dist{ min, max }(gen);
}


class Fish : public Renderable
{
private:
	float Velocity;
	float RotateVelocity;
	unsigned int VAO, VBO[3];

	glm::vec3 wantToGo; 
	int rsteps;
	int steps; 

	glm::vec2 AnglesBeetwen(glm::vec3 v1, glm::vec3 v2);

public:
	Fish();
	~Fish();
	void draw(Shader* sp);
	void behave();
	void move(glm::vec3 coordinates);
};

class Plant : public Renderable
{
public:
	Plant();
	~Plant();
	void behave();
	void draw(Shader* sp);
private:
	unsigned int VAO, VBO[3];
	float location[3];
	
};

class Other : public Renderable
{
private:
	unsigned int VAO, VBO[3];
	float location[3];

public:
	Other(glm::vec3 initialPosition, bool);
	void draw(Shader* sp);
	void behave();

};

class Rock : public Renderable
{
private:
	unsigned int VAO, VBO[3];
public:
	Rock();
	~Rock();
	void draw(Shader* sp);
	void behave();
};

class Bubble : public Renderable
{
private:
	float Velocity;
	unsigned int VAO, VBO[3];
public:
	Bubble(float, float, float);
	~Bubble();
	void draw(Shader* sp);
	void behave();
};

class Wall : public Renderable
{
private:
	unsigned int VAO, VBO[3];
	float location[3];

public:
	Wall(glm::vec3 initialPosition, int);
	void draw(Shader* sp);
	void behave();

};

#endif